import React from 'react';
import { render } from '@testing-library/react';
import Footer from './Footer';

test('renders footer correctly', () => {
    const { getByText } = render(<Footer />);
    const copyrightElement = getByText(/Copyright 2020/);
    expect(copyrightElement).toBeInTheDocument();
});
