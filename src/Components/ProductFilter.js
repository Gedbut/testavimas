import React from "react";
import "../index.css";

export const filterButton = (filter, index, setPrice) => {
  const setPriceFn = () => {
    setPrice(filter.price);
  };
  return (
    <button key={index} className="button" onClick={setPriceFn}>
      {filter.title}
    </button>
  );
};

export const mapFilters = (filters, setPrice) => {
  return filters.map((filter, index) => {
    return filterButton(filter, index, setPrice);
  });
};

const ProductFilter = ({ setPrice }) => {
  const filters = [
    { price: 0, title: "All products" },
    { price: 100, title: ">$100" },
    { price: 200, title: ">$200" },
    { price: 300, title: ">$300" },
  ];
  return (
    <div>
      Filter products by price:
      {mapFilters(filters, setPrice)}
    </div>
  );
};

export default ProductFilter;
