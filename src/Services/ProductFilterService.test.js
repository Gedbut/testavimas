import React from 'react';
import { FilterByPriceAbove } from './ProductFilterService';
const each = require("jest-each").default;

describe("ProductFilterService", () => {
  each([
      [[{ price: 50 }, { price: 80 }, { price: 120 }, { price: 150 }], 0, 4],
      [[{ price: 120 }, { price: 150 }], 0, 2],
      [[{ price: 50 }, { price: 80 }, { price: 120 }, { price: 150 }], 100, 2],
      [[{ price: 120 }, { price: 150 }], 130, 1],
      ]
  ).it('product filter service shows correct amount of products', (arr, price, expected) => {
    const filteredProducts = FilterByPriceAbove(price, arr);
    expect(filteredProducts).toHaveLength(expected);
  })
})