const fs = require("fs");

const path = process.argv[2];

fs.readdir(path, (err, files) => {
  files.forEach((file) => {
    if (file.match(/\./g)?.length === 1 && file.match(/\.*.js$/g)) {
      const fileName = file.substring(0, file.length - 3);
      if (!fs.existsSync(`${path}/${fileName}.test.js`)) {
        fs.writeFile(
          `${path}/${fileName}.test.js`,
          defaultTest(fileName),
          (err) => {
            if (err) return console.log(err);
            console.log(`Generated ${fileName}.test.js`);
          }
        );
      }
    }
  });
});

const defaultTest = (fileName) => {
  return `import React from "react";
import { render } from "@testing-library/react";
import ${fileName} from "./${fileName}";

test("${fileName} generated test", () => {
  //const { getByText } = render(<${fileName} />);
  //const foundElement = getByText("Text in element");
  //expect(foundElement).toBeInTheDocument();
  expect("generated test not implemented").toBe(true);
});`;
};
