import React from "react";
import { render } from "@testing-library/react";
import Home from "./Home";
import { BrowserRouter } from "react-router-dom";

test("renders Home with filter", () => {
  const { getByText } = render(<Home />);
  const filter = getByText(/Filter/);
  expect(filter).toBeInTheDocument();
});

test("renders Home with products", () => {
  const products = {
    loading: false,
    error: false,
    data: [
      {
        createdAt: "2020-04-14T00:02:59.753Z",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut interdum urna nec eleifend aliquet. Aliquam erat volutpat.",
        id: "1",
        images: [
          {
            createdAt: "2020-04-14T17:02:48.574Z",
            id: "1",
            imageUrl: "https://picsum.photos/350/256",
            productId: "1",
          },
        ],
        name: "Awesome Metal Tuna",
        price: "116.00",
      },
    ],
  };
  const { getByText } = render(
    <BrowserRouter>
      <Home predefinedProducts={products} />
    </BrowserRouter>
  );
  const product = getByText(/Lorem ipsum/);
  expect(product).toBeInTheDocument();
});
