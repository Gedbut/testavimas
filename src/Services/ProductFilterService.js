export const FilterByPriceAbove = (price, products) => {
  if (products) {
    return products.filter((product) => product.price >= price);
  }
  return [];
};
