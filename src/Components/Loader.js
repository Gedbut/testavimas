import React from 'react'

function Loader(){
    return (
        <div className="flex justify-center">
            <div className="loader" data-testid="loader"></div>
        </div>
    )
}

export default Loader