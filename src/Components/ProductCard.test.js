import React from 'react';
import ProductCard from './ProductCard';
import { BrowserRouter } from 'react-router-dom';
import { CountLetters } from '../Services/WordsCountService';
import { screen, render } from '@testing-library/react';

test('renders ProductCard component', () => {
  const product = {
    id: '1',
    createdAt: '2020-04-14T00:02:59.753Z',
    name: 'Awesome Metal Tuna',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut interdum urna nec eleifend aliquet. Aliquam erat volutpat.',
    price: '116.00',
    images: [
      {
        id: '1',
        productId: '1',
        createdAt: '2020-04-14T17:02:48.574Z',
        imageUrl: 'https://picsum.photos/350/256',
      },
      {
        id: '61',
        productId: '1',
        createdAt: '2020-04-14T06:44:45.238Z',
        imageUrl: 'https://picsum.photos/350/256',
      },
    ],
  };
  const { getByText } = render(
    <BrowserRouter>
      <ProductCard product={product} />
    </BrowserRouter>
  );
  const linkElement = getByText(/Lorem ipsum/);
  expect(linkElement).toBeInTheDocument();
});

test('count number of letters in description', () => {
  const product = {
    id: '1',
    createdAt: '2020-04-14T00:02:59.753Z',
    name: 'Awesome Metal Tuna',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut interdum urna nec eleifend aliquet. Aliquam erat volutpat.',
    price: '116.00',
    images: [
      {
        id: '1',
        productId: '1',
        createdAt: '2020-04-14T17:02:48.574Z',
        imageUrl: 'https://picsum.photos/350/256',
      },
      {
        id: '61',
        productId: '1',
        createdAt: '2020-04-14T06:44:45.238Z',
        imageUrl: 'https://picsum.photos/350/256',
      },
    ],
  };
  render(
    <BrowserRouter>
      <ProductCard product={product} />
    </BrowserRouter>
  );

  const lengthOfDescription = CountLetters(product.description);

  expect(lengthOfDescription).toBe(118);
});
