import React from 'react';
import {screen, render} from '@testing-library/react';
import NavigationMenu from './NavigationMenu';

test('renders NavigationMenu correctly', () => {
    render(<NavigationMenu />);
    expect(screen.getByText('AppName')).toBeInTheDocument();
    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('About')).toBeInTheDocument();
});
