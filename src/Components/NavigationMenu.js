import React from 'react'
import { Link, BrowserRouter } from "react-router-dom"

function NavigationMenu(props){
    return (
        <div>
            <div className="font-bold py-3">
                AppName
            </div>
            <ul>
                <li>
                    <BrowserRouter>
                    <Link 
                        to="/" 
                        className="text-blue-500 py-3 border-t border-b block"
                        onClick={props.closeMenu}
                    >
                        Home
                    </Link>
                    </BrowserRouter>
                </li>
                <li>
                    <BrowserRouter>
                    <Link 
                        to="/about" 
                        className="text-blue-500 py-3 border-b block"
                        onClick={props.closeMenu}
                    >
                        About
                    </Link>
                    </BrowserRouter>
                </li>
            </ul>
        </div>
    )
}

export default NavigationMenu