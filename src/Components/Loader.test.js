import React from 'react';
import { render } from '@testing-library/react';
import Loader from './Loader';

test('renders loader correctly', () => {
    // const { getByText } = render(<Loader />);
    const { getByTestId } = render(<Loader />);
    const copyrightElement = getByTestId(/loader/);
    expect(copyrightElement).toBeInTheDocument();
});
