import React from 'react';
import {screen, render} from '@testing-library/react';
import Product from './Product';
import {MemoryRouter, Route} from "react-router-dom";

jest.mock('../Hooks/HttpRequests', () => {
    return {
        useAxiosGet: jest.fn().mockReturnValueOnce({}).mockReturnValueOnce({
            data: {
                images: [{
                    imageUrl: 'll',
                }],
                name: 'testName',
                price: 100,
                description: 'testDescription'
            }
        })
    }
})

test('renders About correctly', () => {
    render(
        <MemoryRouter initialEntries={['products/1']}><Route path='products/:id'><Product /></Route></MemoryRouter>);
    expect(screen.getByTestId('product-container')).toBeInTheDocument();
});

test('renders About correctly', () => {
    render(
        <MemoryRouter initialEntries={['products/1']}><Route path='products/:id'><Product /></Route></MemoryRouter>);
    expect(screen.getByTestId('product-name')).toBeInTheDocument();
});