import React, { Component } from 'react';
import Header from './Header';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

test('renders Header component', () => {
  const { getByText } = render(
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  );
  const linkElement = getByText(/AppName/);
  expect(linkElement).toBeInTheDocument();
});
