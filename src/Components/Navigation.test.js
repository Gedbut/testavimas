import React, { useState as useStateMock } from 'react';
import {screen, fireEvent, render, act} from '@testing-library/react';
import Navigation from './Navigation';
import '@testing-library/jest-dom'

jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: jest.fn(),
}))

jest.mock()

const setState = jest.fn()

beforeEach(() => {
    useStateMock.mockImplementation((init = false) => [init, setState])
})

test('renders navigation correctly', () => {
    const { getByText, getByTestId } = render(<Navigation />);
    const iconElement = getByTestId(/awesome-icon/);
    expect(iconElement).toBeInTheDocument();
});

test('Opens navigation hamburger click', () => {
    render(<Navigation />);
    const iconElement = screen.getByTestId(/awesome-icon/);
    act(() => {
        fireEvent.click(iconElement)
    });
    expect(setState).toHaveBeenCalledTimes(1)
    expect(setState).toHaveBeenCalledWith(true)
});
