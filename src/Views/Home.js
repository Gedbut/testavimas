import React, { useState } from 'react';
import Loader from '../Components/Loader';
import ProductCard from '../Components/ProductCard';
import ProductFilter from '../Components/ProductFilter';
import { useAxiosGet } from '../Hooks/HttpRequests';
import { FilterByPriceAbove } from '../Services/ProductFilterService';

function Home({ predefinedProducts }) {
  const [price, setPrice] = useState(0);
  // Create your own Mock API: https://mockapi.io/
  const url = `https://5e9623dc5b19f10016b5e31f.mockapi.io/api/v1/products?page=1&limit=10`;
  let axiosProducts = useAxiosGet(url);

  const products = () => {
    return predefinedProducts ? predefinedProducts : axiosProducts;
  };

  let content = null;

  if (products().data) {
    content = FilterByPriceAbove(price, products().data).map((product) => (
      <div key={product.id} className="flex-no-shrink w-full md:w-1/4 md:px-3">
        <ProductCard product={product} />
      </div>
    ));
  }

  return (
    <div>
      <ProductFilter setPrice={setPrice} />
      <div className="container mx-auto">
        <h1 className="font-bold text-2xl mb-3">Best Sellers</h1>
        <div className="md:flex flex-wrap md:-mx-3">{content}</div>
      </div>
    </div>
  );
}

export default Home;
