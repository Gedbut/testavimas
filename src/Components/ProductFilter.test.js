import React from "react";
import ReactDOMServer from "react-dom/server";
import { render, act, fireEvent } from "@testing-library/react";
import ProductFilter, { filterButton } from "./ProductFilter";

test("render ProductFilter with filters", () => {
  const { getByText } = render(<ProductFilter />);
  const filter = getByText(/All products/);
  expect(filter).toBeInTheDocument();
  expect(filter.parentElement.children).toHaveLength(4);
});

test("ProductFilter sets price filter", () => {
  const setPrice = jest.fn();
  const { getByText } = render(<ProductFilter setPrice={setPrice} />);
  const setTwoHundredButton = getByText(/\$200/);
  act(() => {
    fireEvent.click(setTwoHundredButton);
  });
  expect(setPrice).toHaveBeenCalled();
});

test("filterButton returns button", () => {
  const index = 0;
  const setPrice = null;
  const filter = { title: "test" };
  const button = ReactDOMServer.renderToString(
    <button
      key={index}
      className="button"
      onClick={() => setPrice(filter.price)}
    >
      {filter.title}
    </button>
  );
  const testFilterButton = ReactDOMServer.renderToString(
    filterButton(filter, index, setPrice)
  );
  expect(testFilterButton).toMatch(button);
});
