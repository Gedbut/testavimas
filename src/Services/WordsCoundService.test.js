import React from 'react';
import { CountLetters } from './WordsCountService';

test('Description length should be 24', () => {
  let description = 'Hello, my name is Zenius';

  let length = CountLetters(description);

  expect(length).toBe(24);
});

test('Description length should be 0', () => {
  let description = 1;

  let length = CountLetters(description);

  expect(length).toBe(0);
});
