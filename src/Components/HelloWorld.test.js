import React from 'react';
import { render } from '@testing-library/react';
import HelloWorld from './HelloWorld';

test('renders Hello World', () => {
    const { getByText } = render(<HelloWorld name="testName"/>);
    const helloElement = getByText('Hello testName');
    expect(helloElement).toBeInTheDocument();
});
