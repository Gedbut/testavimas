import React from 'react';
import {screen, render} from '@testing-library/react';
import About from './About';

test('renders About correctly', () => {
    render(<About />);
    expect(screen.getByText('About us')).toBeInTheDocument();
    expect(screen.getByText(/about page content/)).toBeInTheDocument();
});
